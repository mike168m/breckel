const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require("path");
const url = require("url");
const Vue = require("vue");
//const vue_strap = require("vue-strap");

let window = null;

let createWindow = () => {
    console.log("App is ready");
    window = new BrowserWindow({width: 800, height:600});
    window.loadURL("file://"+__dirname+"/index.html");
    createViews();
    window.on("closed", () => {
        console.log("window closed");
    });
};

let createViews = () => {
    console.log("creating views");
    var app = new Vue({
        el: "#app",
        data: {
            message: "Hello Vue"
        }
    }).$mount();
}

app.on("ready",  createWindow);

app.on("activate", ()=>{
    if(window == null){
        createWindow();
    }
});
