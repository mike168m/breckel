#include "scanner.h"

using namespace brekell::frontend;

const std::map<TokenClass, const char*>
Scanner::m_lexemes = {
    {TokenClass::KEYWORD,"KEYWORD"},
    {TokenClass::PRIMITIVE, "PRIMITIVE"},
    {TokenClass::IDENTIFIER, "IDENTIFIER"},
    {TokenClass::BINARY_OPERATOR, "BINARY_OPERATOR"},
    {TokenClass::UNARY_OPERATOR, "UNARY_OPERATOR"},
    {TokenClass::INT_LITERAL, "INT_LITERAL"},
    {TokenClass::FLOAT_LITERAL, "FLOAT_LITERAL"},
    {TokenClass::STRING_LITERAL, "STRING_LITERAL"},
    {TokenClass::LBRACE, "LBRACE"},
    {TokenClass::RBRACE, "RBRACE"},
    {TokenClass::LPAREN, "LPAREN"},
    {TokenClass::RPAREN, "RPAREN"},
    {TokenClass::LSQRBRACKET, "LSQRBRACKET"},
    {TokenClass::RSQRBRACKET, "RSQRBRACKET"},
    {TokenClass::SEMICOLON, "SEMICOLON"},
    {TokenClass::COLON, "COLON"},
    {TokenClass::ASSIGN_OPERATOR, "ASSIGNMENT_OPERATOR"}
};


Scanner::Scanner():m_index(0), m_row(1), m_col(1) {
    m_logger = spdlog::stdout_color_mt("scanner_logger");
    m_logger->set_pattern("[Scanner] %v");
}


/// Starting point of the scanner.
void Scanner::scan(const std::string& characters) {
    char ch = characters[m_index]; // read character

    while (m_index < characters.length()) {
        ch = characters[m_index];
        // if we see a white space character then consume
        // all the following white space chatacters.
        if (isWhiteSpace(ch)) {
            // read all white space characters
            // m_tokens.push_back(scanWhiteSpace(characters));
            scanWhiteSpace(characters);
        }
        else if (isLetter(ch)) {
            // if its a letter scan for identifiers and keywords
            m_tokens.push_back(scanIdentifier(characters));
        }
        else if (isDigit(ch) || ch == '"' || ch == '\'') {
            m_tokens.push_back(scanLiteral(characters));
        }
        else if(ch == '#') {
            scanComment(characters);
        }
        else {
            // Just scan for characters
            // arithmetic operators, braces, etc
            m_tokens.push_back(scanOperator(characters));
        }

        m_col = m_index;
    }

    // reset character indexes
    m_row++;
    m_col = 1;
    m_index = 0;
}


/// Scans for a number, a string literal or a comment
Token Scanner::scanLiteral(const std::string& text){
    std::string literal;
    Token token;

    char ch = text[m_index];
    // scan stirng literals
    if (ch == '"' || ch == '\'') {
        do {
            literal += ch;
            m_index++;
            ch = text[m_index];
        }
        while (ch != '"' && ch != '\'');
        literal += ch;
        m_index += 1;
        token.type = TokenClass::STRING_LITERAL;
        token.typeString = m_lexemes.find(TokenClass::STRING_LITERAL)->second;
    }
    else if (isDigit(ch)) {
        do {
            literal += ch;
            m_index++;
            ch = text[m_index];
        }
        while(isDigit(ch));

       token.type = TokenClass::INT_LITERAL;
       token.typeString = m_lexemes.at(TokenClass::INT_LITERAL);
    }

    token.row = m_row;
    token.col = m_col;
    token.value = literal;
    return token;
}

/// Scans a comment. This just shifts the index unitl the comment ends
void Scanner::scanComment(const std::string& text){
    unsigned counter = m_index;
    while(counter < text.length()) {
        char ch = text[counter];
        if(isPunctuation(ch) || isLetter(ch) ||
            isWhiteSpace(ch) || isDigit(ch))
        {
            m_index++;
            counter++;
        }
        else break;
    }
}

/// Scans for an identifier and classifies it
Token Scanner::scanIdentifier(const std::string& text) {
    std::string identifier;
    // read every subsequent character that is a letter or number
    // and build up the identifier.
    while(true){
        char ch = text[m_index];
        if(!(isLetter(ch) || isDigit(ch))){
            break;
        }

        m_index++;
        identifier += ch;
    }

    // if its a logical operator ... else its a keyword
    if(identifier == "and" || identifier == "or" ) {
        return Token {
                m_row, m_col,
                TokenClass::BINARY_OPERATOR,
                m_lexemes.find(TokenClass::BINARY_OPERATOR)->second,
                identifier
            };
    }
    else if ( identifier == "fxn"    || identifier == "object"     ||
             identifier == "if"      || identifier == "elseif"     ||
             identifier == "else"    || identifier == "instanceof" ||
             identifier == "let"     || identifier == "const"      )
    {
        return Token {
                m_row, m_col,
                TokenClass::KEYWORD,
                m_lexemes.find(TokenClass::KEYWORD)->second,
                identifier
            };
    }
    else if( identifier == "int"    || identifier == "float" ||
             identifier == "string" )
    {
        return Token {
                m_row, m_col,
                TokenClass::PRIMITIVE,
                m_lexemes.find(TokenClass::PRIMITIVE)->second,
                identifier
            };
    }
    else if (identifier == "not"){
        return Token {
                m_row, m_col,
                TokenClass::UNARY_OPERATOR,
                m_lexemes.find(TokenClass::UNARY_OPERATOR)->second,
                identifier
            };
    }

    // else its an identifier
    return Token {
            m_row, m_col,
            TokenClass::IDENTIFIER,
            m_lexemes.find(TokenClass::IDENTIFIER)->second,
            identifier
        };
}


/// Scans for an operator
Token Scanner::scanOperator(const std::string& text) {
    Token token;
    token.col = m_col;
    token.row = m_row;

    char ch = text[m_index];
    char nextch = text[m_index + 1];
    if(ch == '-' && nextch == '>'){
        token.type = TokenClass::BINARY_OPERATOR;
        token.typeString =
                m_lexemes.find(TokenClass::BINARY_OPERATOR)->second;
        token.value = {ch, nextch};
        m_index++;
    }
    else{
        switch(ch){
        case '-':
        case '*':
        case '/':
        case '%':
        case '+':
        case '^':
        case '<':
        case '>':
        case ',':
        case '|':
        case '&':
            token.type = TokenClass::BINARY_OPERATOR;
            token.typeString =
                    m_lexemes.find(TokenClass::BINARY_OPERATOR)->second;
            token.value = ch;
            break;
        case ':':
            token.type = TokenClass::COLON;
            token.typeString = m_lexemes.find(TokenClass::COLON)->second;
            token.value = ch;
            break;
        case ';':
            token.type = TokenClass::SEMICOLON;
            token.typeString = m_lexemes.find(TokenClass::SEMICOLON)->second;
            token.value = ";";
            break;
        case '{':
            token.type = TokenClass::LBRACE;
            token.typeString = m_lexemes.find(TokenClass::LBRACE)->second;
            token.value = "{";
            break;
        case '}':
            token.type = TokenClass::RBRACE;
            token.typeString = m_lexemes.find(TokenClass::RBRACE)->second;
            token.value = "}";
            break;
        case '(':
            token.type = TokenClass::LPAREN;
            token.typeString = m_lexemes.find(TokenClass::LPAREN)->second;
            token.value = "(";
            break;
        case ')':
            token.type = TokenClass::RPAREN;
            token.typeString = m_lexemes.find(TokenClass::RPAREN)->second;
            token.value = ")";
            break;
        case '[':
            token.type = TokenClass::LSQRBRACKET;
            token.typeString = m_lexemes.find(TokenClass::LSQRBRACKET)->second;
            token.value = "[";
            break;
        case ']':
            token.type = TokenClass::RSQRBRACKET;
            token.typeString = m_lexemes.find(TokenClass::RSQRBRACKET)->second;
            token.value = "]";
            break;
        case '=':
            token.type = TokenClass::ASSIGN_OPERATOR;
            token.typeString =
                    m_lexemes.find(TokenClass::ASSIGN_OPERATOR)->second;
            token.value = "=";
            break;
        default:
            break;
        }
    }

    m_index++;
    return token;
}


// Shift index while exhausting whitespace characters
void Scanner::scanWhiteSpace(const std::string& text){
    while (true) {
        char ch = text[m_index];
        if (isWhiteSpace(ch)) {
            m_index++;
        }
        else break;
    }
}


// Check if character is a digit
inline bool Scanner::isDigit(char ch) const noexcept {
    return ch >= '0' && ch <= '9';
}

// Check if character is an alphabet
inline bool Scanner::isLetter(char ch) const noexcept {
    return (ch >= 'a' && ch <= 'z') |
           (ch >= 'A' && ch <= 'Z') |
           (ch == '_') || (ch == '$');
}

// Check if character is white space
inline bool Scanner::isWhiteSpace(char ch) const noexcept {
    return (ch == ' ') || (ch == '\n') || (ch == '\t');
}

// Check if character is a punctuation. Is used in scanComment()
// Uses <cctype> ispunct.
inline bool Scanner::isPunctuation(char ch) const noexcept {
    return ispunct(ch) != 0;
}

// Print all tokens in the token list
// with their row and column
void Scanner::print() {
    for (const Token& tk : m_tokens) {
        m_logger->info("{}", tk);
    }
    m_logger->set_pattern("");
    m_logger->info('\n');
}

// Returns scanned tokens
std::vector<Token>& Scanner::tokens() {
    if (m_tokens.size() <= 0) {
        throw std::runtime_error("Scanning failed! There were no tokens to process.");
    }
    if ((*m_tokens.rbegin()).type != TokenClass::END) {
        m_tokens.push_back(Token{0,0, TokenClass::END, "END", "EOF"});
    }
    return this->m_tokens;
}

