#pragma once

#include <map>
#include <queue>
#include <better_enums/enum.h>
#include <experimental/optional>
#include "ast.h"
#include "parser.h"
#include <algorithm>

namespace brekell {
namespace semantics {

BETTER_ENUM (SYMBOL_CATEGORY, unsigned,
    NONE = 0,
    FUNCTION,
    VARIABLE
)

class SymbolTableBuilder : public Stage {
private:
    struct Symbol {
        SYMBOL_CATEGORY category;
        unsigned long refCount;
        const char* name;
        const char* type;

        Symbol()
            :category(SYMBOL_CATEGORY::NONE),
             refCount(0), name(""), type("")
        {}

        template<typename OStream>
        friend OStream& operator<<(OStream& os, const Symbol &s) {
            return os << "{ name: " << s.name
                      << ", category: " << s.category._to_string()
                      << ", ref_cnt: " << s.refCount << "}";
        }
    };

    using SymbolPtr = std::unique_ptr<Symbol>;

    class SymbolTable {
    public:
        std::unordered_map<std::string, Symbol> symbols;
        void insert(const Symbol& symbol) {
            if(std::strcmp(symbol.name, "") != 0) {
                symbols.insert(std::make_pair(symbol.name, symbol));
            }
        }

        Symbol& lookup(const char* name) {
            static Symbol emptySymbol;
            auto&& ref = symbols.find(name);
            if (ref != symbols.end()) {
                return ref->second;
            }
            return emptySymbol; // empty
        }
    };

    brekell::frontend::Ast& m_ast;
    SymbolTable m_table;
    //std::shared_ptr<spdlog::logger> m_logger;

public:
    SymbolTableBuilder(brekell::frontend::Ast& ast)
        :m_ast(ast)
    {
        m_logger = spdlog::stdout_color_mt("SymTbl");
        m_logger->set_pattern("[SymTbl] %v");
    }

    void build() {
        using namespace brekell::frontend;
        assert(m_ast.root->children.size() > 0);
        std::queue<NodePtr> nodeStack;
        nodeStack.push(m_ast.root);
        while (!nodeStack.empty()) {
            // process node top of stack.
            brekell::frontend::NodePtr node = nodeStack.front();
            // then pop this node of the stack
            // and push its children
            nodeStack.pop();
            for (auto& child : node->children) {
                nodeStack.push(child);
            }

            Symbol newSymbol;
            newSymbol.type = ""; // no type. Is filled by type cheker.

            if (node->type == +NODE_TYPE::LET) {
                newSymbol.name = node->lastChild()->firstChild()
                                     ->firstChild()->tokens[0].value.c_str();
                newSymbol.category = SYMBOL_CATEGORY::VARIABLE;
                m_table.insert(newSymbol);
            }
            else if (node->type == +NODE_TYPE::IDENTIFIER_DECL) {
                newSymbol.name = node->firstChild()->tokens[0].value.c_str();
                newSymbol.category = SYMBOL_CATEGORY::VARIABLE;
                m_table.insert(newSymbol);
            }
            else if (node->type == +NODE_TYPE::FUNCTION) {
                newSymbol.name = node->tokens[0].value.c_str();
                newSymbol.category = SYMBOL_CATEGORY::FUNCTION;
                m_table.insert(newSymbol);
            }
            else if (node->type == +NODE_TYPE::IDENTIFIER_REFERENCE
                     || node->type == +NODE_TYPE::FUNCTION_CALL)
            {
                const char* name = node->tokens[0].value.c_str();
                Symbol& s = m_table.lookup(name);
                if (std::strcmp(s.name, "") == 0) {
                    m_logger->error("identifier {} referenced " \
                                     "before declaration.",
                                     node->tokens[0]);
                }
                s.refCount += 1;
            }
        }

    }

    SymbolTable& table() noexcept {
        return this->m_table;
    }

    void print() override {
        for (auto& pair: m_table.symbols) {
           m_logger->info("{} => {}",  pair.first, pair.second);
        }
        m_logger->info("Done.\n");
    }
};

} //semantics
} // brekell
