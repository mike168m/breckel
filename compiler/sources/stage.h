#include <iostream>
#include <memory>
#include <spdlog/spdlog.h>


namespace brekell{
class Stage {
protected:
    std::shared_ptr<spdlog::logger> m_logger;
public:
    virtual void print() {
        m_logger->info("nothing to print out for this stage.");
    }
};
}
