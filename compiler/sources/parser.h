// ... GPL License
#pragma once

#include <vector>
#include <iterator>
#include <memory>
#include <exception>
#include <spdlog/spdlog.h>
#include <better_enums/enum.h>
#include "scanner.h"
#include "ast.h"

namespace brekell {
namespace frontend {

using vecIter = std::vector<Token>::iterator;


class Parser : public Stage {
private:
    unsigned m_numErrors;
    std::vector<Token>& m_tokens; // holds tokens for a module
    vecIter m_iter; // points to the next word
    //std::shared_ptr<spdlog::logger> m_logger;

    // we need to have a list of known tokens
    // cuz all the scanner does is 'classify'
    // tokens
    enum class KNOWN_TOKEN {
        FXN,
        LET,
        FOR,
        SEMICOLON,
        LBRACE,
        RBRACE,
        LPAREN,
        RPAREN,
        COLON,
        OP_MAP,
        OP_ADD,
        OP_SUB,
        OP_MUL,
        OP_DIV,
        OP_BOR,
        OP_BAND,
        OP_ADDR,
        OP_ASSIGN,
        OP_VSEP,
        COMMA,
        KEYWORD_BYTE,
        LSQRBRACKET,
        RSQRBRACKET,
        KEYWORD_INT,
        KEYWORD_FLOAT,
        KEYWORD_STRING,
    };

    enum class ERROR_LEVEL {
        E_WARNING, // warn user. Mostly for ambiguities in expressions
        E_SEVERE,  // print error message and stop parsing
        E_ERROR // continue parsing but print error message
    };

    // resulting AST
    Ast m_tree;
    int m_indent;
    // mapping of knonw tokens
    static std::map<KNOWN_TOKEN, const char*>
    m_tokenValues;

    using kt = Parser::KNOWN_TOKEN;

    // types of expressions
    void consume() {std::advance(m_iter, 1);}

    bool match(TokenClass type) {
        return (*m_iter).type == type;
    }

    bool match(Parser::KNOWN_TOKEN v) {
        return m_tokenValues[v] == (*m_iter).value;
    }

    template<typename T = const char*>
    void expect(T c) {
        if((*m_iter).value == c) {
            consume();
        }
        else {
            error(std::string("Expected ") + c
                  + std::string(" but got ")
                  + (*m_iter).value,
                  ERROR_LEVEL::E_ERROR);
        }
    }

    template<typename T = const char*, typename... Args >
    void expect(T first, Args... args) {
        expect(first);
        expect(args...);
    }

// use c++17 fold expression if possible.
#if  __cpp_fold_expressions
    template<typename... Args>
    bool isAnyOf(Args... args) {
        return (... || match(args));
    }
#else
    template<typename T>
    bool isAnyOf(T f){
        return match(f);
    }

    template<typename first, typename... Args>
    bool isAnyOf(first f, Args... args){
        return isAnyOf(f) || isAnyOf(args...);
    }
#endif

    // arithmetic expressions
    bool parseArithmeticExpressionPrime(NodePtr& node);
    bool parseArithmeticExpressionTerm(NodePtr& node);
    bool parseArithmeticExpressionTermPrime(NodePtr& node);

    // boolean expressions
    bool parseBooleanExpressionPrime(NodePtr& node);
    bool parseBooleanExpressionTerm(NodePtr& node);
    bool parseBooleanExpressionTermPrime(NodePtr& node);

    // function expressions
    bool parseFxnArguments(NodePtr& node);
    bool parseFxnArgumentList(NodePtr& node);
    bool parseFxnMoreArgumentList(NodePtr& node);

    bool parseArguments(NodePtr& node);
    bool parseArgumentList(NodePtr& node);
    bool parseMoreArgumentList(NodePtr& node);
    bool parseIdentifierDecl(NodePtr& node);
    bool parseTypeName(NodePtr& node, bool fromFunction = false);
    bool parseFactor(NodePtr& node);

    void printAST_impl(unsigned d, unsigned f, NodePtr& node);
public:
    explicit Parser(std::vector<Token>& tokens, unsigned indent = 2);
    ~Parser() = default;

   void parse();
   bool parseExpressionChain(NodePtr& node);
   bool parseExpression(NodePtr& node);
   bool parseArithmeticExpression(NodePtr& node);
   bool parseBooleanExpression(NodePtr& node);
   bool parseConditionalExpression(NodePtr& node);
   bool parseLoopExpression(NodePtr& node);
   bool parseFxnExpression(NodePtr& node);
   bool parsePrimaryAssignmentExpression(NodePtr& node);
   bool parseSecondaryAssignmentExpression(NodePtr& node, bool fromPrimary = false);
   void error(const std::string& args,
              ERROR_LEVEL elevel = ERROR_LEVEL::E_SEVERE);

   virtual void print() override;
   Ast& ast();
};

} // frontend
} // brekell

