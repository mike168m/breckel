// ... GPL License
#pragma once

#include <iostream>
#include <sstream>
#include <map>
#include <utility>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>
#include "stage.h"

namespace brekell {
namespace frontend {

enum class TokenClass {
    KEYWORD,
    PRIMITIVE,
    COLON,
    IDENTIFIER,
    BINARY_OPERATOR,
    UNARY_OPERATOR,
    ASSIGN_OPERATOR,
    INT_LITERAL,
    STRING_LITERAL,
    FLOAT_LITERAL,
    RBRACE,
    LBRACE,
    RPAREN,
    LPAREN,
    LSQRBRACKET,
    RSQRBRACKET,
    SEMICOLON,
    END
};

struct Token {
    unsigned row;
    unsigned col;
    TokenClass type;
    std::string typeString;
    std::string value;

    const char* toString() const {
        return std::string(row + ',' + col + " " + value).c_str();
    }

    bool operator==(Token& rref) {
        return (this->value == rref.value) && (this->type == rref.type);
    }

    template<typename OStream>
    friend OStream& operator<<(OStream& os, const Token &t) {
        return os << '<' << t.row << ',' << t.col << ','
                  << t.typeString << ',' << std::quoted(t.value) << '>';
    }
};

class Scanner : public brekell::Stage {
private:
    unsigned int m_index;
    unsigned int m_row;
    unsigned int m_col;

    std::vector<Token> m_tokens;
    static const std::map<TokenClass, const char*> m_lexemes;
    //std::shared_ptr<spdlog::logger> m_logger;

public:
    explicit Scanner();
    ~Scanner() = default;
    void scan(const std::string& charStream);
    void scanWhiteSpace(const std::string& text);
    Token scanIdentifier(const std::string& text);
    Token scanOperator(const std::string& text);
    Token scanLiteral(const std::string& text);
    void scanComment(const std::string& text);
    bool isPunctuation(char ch) const noexcept;
    bool isWhiteSpace(char ch) const noexcept;
    bool isLetter(char ch) const  noexcept;
    bool isDigit(char ch) const noexcept;
    virtual void print() override;
    std::vector<Token>& tokens();
};

}}

