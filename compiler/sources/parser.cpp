// ... GPL License
#include "parser.h"

using namespace brekell::frontend;

// Known token values to be used with
std::map<Parser::KNOWN_TOKEN, const char*> Parser::m_tokenValues = {
    { Parser::KNOWN_TOKEN::COMMA, ","},
    { Parser::KNOWN_TOKEN::COLON, ":"},
    { Parser::KNOWN_TOKEN::FXN,   "fxn"},
    { Parser::KNOWN_TOKEN::LET,   "let"},
    { Parser::KNOWN_TOKEN::FOR,   "for"},
    { Parser::KNOWN_TOKEN::LBRACE, "{"},
    { Parser::KNOWN_TOKEN::RBRACE, "}"},
    { Parser::KNOWN_TOKEN::LPAREN, "("},
    { Parser::KNOWN_TOKEN::RPAREN, ")"},
    { Parser::KNOWN_TOKEN::OP_MAP, "->"},
    { Parser::KNOWN_TOKEN::OP_ADD, "+"},
    { Parser::KNOWN_TOKEN::OP_SUB, "-"},
    { Parser::KNOWN_TOKEN::OP_MUL, "*"},
    { Parser::KNOWN_TOKEN::OP_DIV, "/"},
    { Parser::KNOWN_TOKEN::OP_ADDR, "&" },
    { Parser::KNOWN_TOKEN::OP_ASSIGN, "="},
    { Parser::KNOWN_TOKEN::SEMICOLON, ";"},
    { Parser::KNOWN_TOKEN::LSQRBRACKET, "["},
    { Parser::KNOWN_TOKEN::RSQRBRACKET, "]"},
    { Parser::KNOWN_TOKEN::OP_BOR, "or"},
    { Parser::KNOWN_TOKEN::OP_BAND, "and"},
    { Parser::KNOWN_TOKEN::OP_VSEP, "|"}
};


// Constructs a parser
// will initialize a logger too, for reporting errors
Parser::Parser(std::vector<Token>& tokens, unsigned indent)
       : m_numErrors(0), m_tokens(tokens), m_indent(indent)
{
    m_iter = m_tokens.begin();
    m_logger = spdlog::stdout_color_mt("parser_logger");
    m_logger->set_pattern("[Parser] %v");
}

// Returns resulting abstact syntax tree.
Ast& Parser::ast() {
    return this->m_tree;
}

// Prints the nodes of the tree in pre-order
void Parser::print() {
    unsigned depth = 1;
    m_logger->info("");
    std::string delim(depth * m_indent, '_');
    m_logger->set_pattern("%v");
    for (auto& node : m_tree.root->children) {
        m_logger->info("|{}{}", delim, *node);
        printAST_impl(depth, m_indent, node);
    }
    m_logger->info('\n');
    m_logger->set_pattern("[Parser] %v");
}

void Parser::printAST_impl(unsigned depth, unsigned factor, NodePtr& tree){
    depth  += 1;
    std::string childDelim(depth * factor, '_');

    for (auto& node : tree->children) {
        m_logger->info("|{}{}", childDelim, *node);
        if (node->children.size() > 0) {
            printAST_impl(depth, factor, node);
        }
    }
}


// call to start parsing
void Parser::parse() {
    if(!parseExpressionChain(m_tree.root)) {
        error("Invalid expression chain.");
    }
}


// Parses with the following grammar
// ExpChain -> Expr ; ExpChain
//           | EPSILON
bool Parser::parseExpressionChain(NodePtr& tree) {
    if(!match(TokenClass::END))
    {
        if (isAnyOf(kt::FXN,
                    TokenClass::INT_LITERAL,
                    TokenClass::STRING_LITERAL,
                    TokenClass::IDENTIFIER,
                    TokenClass::SEMICOLON,
                    TokenClass::LBRACE,
                    TokenClass::LPAREN,
                    kt::LET,
                    kt::FOR))
        {
            if (parseExpression(tree)) {
                expect(m_tokenValues[kt::SEMICOLON]);
                return parseExpressionChain(tree);
            }
            else if(isAnyOf(TokenClass::END, TokenClass::RBRACE)) {
                return true;
            }
            else {
                error("End of expression, ';' expected");
                return false;
            }
        }
        else if(isAnyOf(TokenClass::END, TokenClass::RBRACE,
                        TokenClass::RPAREN))
        {
            return true;
        }
        else {
            error("Invalid expression chain.",ERROR_LEVEL::E_ERROR);
            return false;
        }
    }

    return true;
}

// Parses with the following grammar
// Exp -> FExp | AExp | ASSIGNExp | EPSILON
inline bool Parser::parseExpression(NodePtr& tree) {
    if(parseFxnExpression(tree)
       || parsePrimaryAssignmentExpression(tree)
       || parseSecondaryAssignmentExpression(tree)
                   || parseBooleanExpression(tree)
       || parseArithmeticExpression(tree)
)
    {
        return true;
    }
    else if(isAnyOf(TokenClass::RPAREN, TokenClass::SEMICOLON,
                    kt::COMMA, kt::OP_VSEP))
    {
        consume();
        return true;
    }

    return false;
}


// ============== Function Expressions ==============
bool Parser::parseFxnExpression(NodePtr& tree){
    // use more look aheads here because of ambiguous
    // on Exp -> AEXP | FEXP
    Token& l1 = *(m_iter + 1);
    Token& l2 = *(m_iter + 2);
    if(match(TokenClass::IDENTIFIER)
            && l1.value == m_tokenValues[kt::COLON]
            && l2.value == m_tokenValues[kt::FXN])
    {
        // get function name for errors and such
        const char* fname = (*m_iter).value.c_str();
        consume();
        expect(m_tokenValues[kt::COLON],
                m_tokenValues[kt::FXN]);

        // create function node with 'name'
        NodePtr fxnNode(new Ast::Node(NODE_TYPE::FUNCTION, *(m_iter - 3)));

        NodePtr argsNode(new Ast::Node(NODE_TYPE::FUNCTION_ARGS));

        if (parseFxnArguments(argsNode))
        {
            // make arguments node child of fxnNode
            fxnNode->addChildren(argsNode);
            expect(m_tokenValues[kt::OP_MAP]);
            if(parseTypeName(fxnNode, true))
            {
                expect(m_tokenValues[kt::LBRACE]);
                // create function body node
                NodePtr bodyNode(new Ast::Node(NODE_TYPE::FUNCTION_BODY));
                if(parseExpressionChain(bodyNode)) {
                    expect(m_tokenValues[kt::RBRACE]);
                    // add body node function
                    fxnNode->addChildren(bodyNode);
                    // finally add function node to subtree.
                    tree->addChildren(fxnNode);
                    return true;
                }
            }
        }
        else {
            error("Expected a valid argument list for "+std::string(fname));
        }
    }

    return false;
}

// ============== Boolean expressions ==============
bool Parser::parseBooleanExpression(NodePtr&  tree) {
    if (isAnyOf(kt::LPAREN, kt::LBRACE,
                TokenClass::INT_LITERAL,
                TokenClass::STRING_LITERAL,
                TokenClass::IDENTIFIER,
                kt::LET, kt::FOR, kt::OP_BAND,
                kt::OP_BOR))
    {
        if (parseBooleanExpressionTerm(tree)) {
            return parseBooleanExpressionPrime(tree);
        }
        return false;
    }
    return false;
}

bool Parser::parseBooleanExpressionPrime(NodePtr& tree){
    if(match(kt::OP_BOR)) {
        consume();
        if (parseBooleanExpressionTerm(tree)) {
            return parseBooleanExpressionPrime(tree);
        }
        return false;
    }
    else if (isAnyOf(kt::OP_VSEP,
                     kt::COMMA,
                     TokenClass::RPAREN,
                     TokenClass::SEMICOLON)) {
        return true;
    }
    return false;
}

bool Parser::parseBooleanExpressionTerm(NodePtr&  tree) {
    if (isAnyOf(kt::LPAREN, kt::LBRACE,
                TokenClass::INT_LITERAL,
                TokenClass::STRING_LITERAL,
                TokenClass::IDENTIFIER,
                kt::LET, kt::FOR, kt::OP_BAND))
    {
        if (parseFactor(tree)) {
            return parseBooleanExpressionTermPrime(tree);
        }
        return false;
    }
    return false;
}

bool Parser::parseBooleanExpressionTermPrime(NodePtr&  tree) {
    if(match(kt::OP_BAND)) {
        consume();
        if (parseFactor(tree)){
            return parseBooleanExpressionTermPrime(tree);
        }
        return false;
    }
    else if (isAnyOf(kt::OP_BOR,
                     kt::OP_VSEP,
                     kt::COMMA,
                     TokenClass::RPAREN,
                     TokenClass::SEMICOLON))
    {
        return true;
    }
    return false;
}


// ============== Assignment expressions ============
//
// Parses with the following right recursive grammar
// PASSIGN_EXP → 'let' SASSIGN_EXP
bool Parser::parsePrimaryAssignmentExpression(NodePtr& tree) {
    if (match(kt::LET)) {
        // create let node
        NodePtr letNode(new Ast::Node(NODE_TYPE::LET));
        letNode->tokens.push_back(*m_iter);
        tree->children.push_back(letNode);
        consume();
        return parseSecondaryAssignmentExpression(letNode, true);
    }
    else if (isAnyOf(kt::OP_VSEP, kt::COMMA,
                     TokenClass::SEMICOLON,
                     TokenClass::RPAREN))
    {
        return true;
    }

    return false;
}


// Parses with the following right recursive grammar
// SASSIGN_EXP -> 'name' = Exp
bool Parser::parseSecondaryAssignmentExpression(NodePtr& tree, bool fromPrimary){
    Token& l1 = *(m_iter + 1); // look ahead one to avoid conflict
    if (match(TokenClass::IDENTIFIER)
         && l1.value == m_tokenValues[kt::OP_ASSIGN])
    {
        // [let] -> [=] -> [left:iden, right:Exp ]
        NodePtr idenNode;

        if (fromPrimary) {
            idenNode = std::make_shared<Ast::Node>
                                    (NODE_TYPE::IDENTIFIER_DECL);
        }
        else{
            idenNode = std::make_shared<Ast::Node>
                                  (NODE_TYPE::IDENTIFIER_REFERENCE);
        }
        idenNode->addChildren(NodePtr(
                                  new Ast::Node(NODE_TYPE::IDENTIFIER,*m_iter)));
        consume();
        // should have a `=` is valid
        expect(m_tokenValues[kt::OP_ASSIGN]);
        // we found the assignment operator
        NodePtr assignNode(new Ast::Node(NODE_TYPE::OP_ASSIGN));
        assignNode->tokens.push_back(*(m_iter - 1));
        // get the assignment op we just consumed
        // make let node the parent
        tree->children.push_back(assignNode);
        // add iden to left most child of assign node
        assignNode->children.push_back(idenNode);
        // get right hand tree.
        return parseExpression(assignNode);
    }

    return false;
}

// ============== Arithmetic expressions ============
//
// Parses with the following right recursive grammar
// Exp -> Term ExpPrime
inline bool Parser::parseArithmeticExpression(NodePtr& tree) {
    if(parseArithmeticExpressionTerm(tree)){
        return parseArithmeticExpressionPrime(tree);
    }

    return false;
}


// Parses with the following right recursive grammar
// ExpPrime -> + Term ExpPrime
//              | - Term ExpPrime
//           | END
// lowest precedence level
bool Parser::parseArithmeticExpressionPrime(NodePtr& tree) {
    if (match(kt::OP_ADD) || match(kt::OP_SUB)) {
        // ExpPrime -> + Term ExPrime
        // ExpPrime -> - Term ExpPrime
        NodePtr opNode(new Ast::Node(NODE_TYPE::OP_ARITHMETIC_BINARY));
        opNode->tokens.push_back(*m_iter);
        // make the tree right node the child of
        // opNode and remove it from the this tree's child list
        opNode->addChildren(tree->lastChild());
        tree->removeLastChild();
        tree->addChildren(opNode);
        // can consume now.
        consume();
        if (parseArithmeticExpressionTerm(opNode)) {
            return parseArithmeticExpressionPrime(opNode);
        }
        //error("Warning! ambiguous add/sub operation",
        // ERROR_LEVEL::WARNING);
        return false;
    }
    else if (isAnyOf(kt::COMMA,
                     TokenClass::RSQRBRACKET,
                     TokenClass::RPAREN,
                     TokenClass::SEMICOLON,
                     kt::OP_VSEP))
    {
        // ExpPrime -> END
        return true;
    }
    // Have to return true even if it false
    return false;
}


// Parses with the following right recursive grammar
// Term -> Factor TermPrime
inline bool Parser::parseArithmeticExpressionTerm(NodePtr& tree) {
    if(parseFactor(tree)){
        return parseArithmeticExpressionTermPrime(tree);
    }
    return false;
}


// Parses with the following right recursive grammar
// TermPrime -> * Factor TermPrime
//            | / Factor TermPrime
//            | END
bool Parser::parseArithmeticExpressionTermPrime(NodePtr& tree) {
    if (match(kt::OP_MUL) || match(kt::OP_DIV)) {
        // ExpPrime -> * Term ExPrime
        // ExpPrime -> / Term ExpPrime
        NodePtr opNode(new Ast::Node(NODE_TYPE::OP_ARITHMETIC_BINARY));
        opNode->tokens.push_back(*m_iter);
        // make the tree right node the child of
        // opNode and remove it from the this tree's child list
        opNode->children.push_back(tree->children.back());
        tree->children.pop_back();
        tree->children.push_back(opNode);
        // can consume now.
        consume();

        if (parseFactor(opNode)) {
            return parseArithmeticExpressionTermPrime(tree);
        }
        return false;
    }
    else if ( isAnyOf(kt::OP_ADD,
                      kt::OP_SUB,
                      kt::COMMA,
                      kt::OP_VSEP,
                      TokenClass::RPAREN,
                      TokenClass::SEMICOLON,
                      TokenClass::RSQRBRACKET))
    {
        return true;
    }

    // error("Warning! ambiguous arithemtic operation(s)", ERROR_LEVEL::E_WARNING);
    return false;
}


// Parses with the following right recursive grammar
// Factor -> \( ExpChain \)
//         |  identifier
//         |  identifier Args
//         |  num
bool Parser::parseFactor(NodePtr& tree) {
    if (match(TokenClass::LPAREN)) {
        consume();
        if(!parseExpression(tree)) {
            error("Expected a literal or identifier in expression.",
                  ERROR_LEVEL::E_ERROR);
            return false;
        }
        if(!match(TokenClass::RPAREN)){
            error("Expected a ')'", ERROR_LEVEL::E_ERROR);
        }

        consume();
        return true;
    }
    else if (match(TokenClass::LBRACE)) {
        // make new scope node
        NodePtr newScopeNode(new Ast::Node(NODE_TYPE::EXPLICIT_SCOPE));
        consume();
        if (parseExpressionChain(newScopeNode)) {
            expect(m_tokenValues[kt::RBRACE]);
            tree->addChildren(newScopeNode);
            return true;
        }
    }
    else if(match(TokenClass::INT_LITERAL)) {
        // create integer node
        NodePtr numNode(new Ast::Node(NODE_TYPE::INT_LITERAL));
        numNode->addTokens(*m_iter);
        // add numNode as tree child
        tree->children.push_back(numNode);
        consume();
        return true;
    }
    else if(match(TokenClass::STRING_LITERAL)) {
        // create integer node
        NodePtr stringNode(new Ast::Node(NODE_TYPE::STRING_LITERAL));
        stringNode->addTokens(*m_iter);
        // add string literal as tree child
        tree->children.push_back(stringNode);
        consume();
        return true;
    }
    else if (match(TokenClass::IDENTIFIER)) {
        // create integer node
        NodePtr numNode(new Ast::Node(NODE_TYPE::IDENTIFIER_REFERENCE));
        numNode->addTokens(*m_iter);
        // add identifier as tree child
        tree->addChildren(numNode);
        consume(); // consume first
        return parseArguments(tree);
    }

    return false;
}


bool Parser::parseFxnArguments(NodePtr& tree){
    if (match(TokenClass::LPAREN)) {
        consume();
        if (!parseFxnArgumentList(tree)) {
            error("Invalid function args declaration ...");
            return false;
        }
        expect(m_tokenValues[kt::RPAREN]);
        return true;
    }

    return false;
}

bool Parser::parseFxnArgumentList(NodePtr& tree) {
    if (match(TokenClass::IDENTIFIER)) {
        if (parseIdentifierDecl(tree)) {
            return parseFxnMoreArgumentList(tree);
        }
        else{
            error("Invalid function arguments declaration ...",
                  ERROR_LEVEL::E_ERROR);
            return false;
        }
    }
    else if (match(TokenClass::RPAREN)) {
        return true;
    }

    return false;
}

bool Parser::parseFxnMoreArgumentList(NodePtr& tree) {
    if (match(kt::COMMA)) {
        consume();
        if(parseIdentifierDecl(tree)){
            return parseFxnMoreArgumentList(tree);
        }
    }
    else if(match(TokenClass::RPAREN)) {
        return true;
    }

    return false;
}

bool Parser::parseIdentifierDecl(NodePtr& tree) {
    if (match(TokenClass::IDENTIFIER)) {
        consume();
        expect(m_tokenValues[kt::COLON]);
        if (parseTypeName(tree)) {
            NodePtr idenDeclNode(new Ast::Node(NODE_TYPE::IDENTIFIER_DECL));
            NodePtr idenNode(new Ast::Node(NODE_TYPE::IDENTIFIER,
                                           *(m_iter - 3))); // name
            NodePtr idenTypeNode(new Ast::Node(NODE_TYPE::IDENTIFIER_TYPE,
                                               *(m_iter - 1))); // type
            idenDeclNode->addChildren(idenNode, idenTypeNode);
            tree->addChildren(idenDeclNode);
            return true;
        }
    }

    return false;
}

bool Parser::parseArguments(NodePtr& tree) {
    if(match(TokenClass::LSQRBRACKET)) {
        consume();
        NodePtr indexOperatorNode(new Ast::Node(NODE_TYPE::OP_INDEX));
        if (!parseArithmeticExpression(indexOperatorNode)) {
            return false;
        }
        tree->addChildren(indexOperatorNode);

        expect(m_tokenValues[kt::RSQRBRACKET]);
        return true;
    }
    else if (match(TokenClass::LPAREN)) {
        consume();
        NodePtr fxnCallNode(new Ast::Node(NODE_TYPE::FUNCTION_CALL));
        if (!parseArgumentList(fxnCallNode)) {
            return false;
        }
        // use vector overload to add to token list
        fxnCallNode->addTokens(tree->lastChild()->tokens);
        // remove that child from the parent node
        tree->removeLastChild();
        // finally add function call node to parent node.
        tree->addChildren(fxnCallNode);
        // should have a '}' if correct
        expect(m_tokenValues[kt::RPAREN]);
        return true;
    }
    else if(isAnyOf(kt::OP_MUL, kt::OP_DIV,
                    kt::OP_ADD, kt::OP_SUB,
                    kt::OP_BAND, kt::OP_BOR,
                    TokenClass::LSQRBRACKET,
                    TokenClass::RPAREN,
                    TokenClass::SEMICOLON,
                    kt::OP_VSEP))
    {
        return true;
    }

    return false;
}


bool Parser::parseArgumentList(NodePtr& tree){
    if(isAnyOf(TokenClass::IDENTIFIER,
               TokenClass::INT_LITERAL,
               TokenClass::LPAREN,
               TokenClass::LBRACE,
               kt::COMMA,
               kt::LET,
               kt::FOR))
    {
        if (parseExpression(tree)) {
            return parseMoreArgumentList(tree);
        }
    }
    else if(match(TokenClass::RPAREN)) {
        return true;
    }

    return false;
}


bool Parser::parseMoreArgumentList(NodePtr& tree){
    if (match(kt::COMMA)) {
        consume();
        if (parseExpression(tree)) {
            return parseMoreArgumentList(tree);
        }
    }
    else if (match(TokenClass::RPAREN)) {
        return true;
    }

    return false;
}

bool Parser::parseTypeName(NodePtr& tree, bool fromFunction){
    if (isAnyOf(TokenClass::IDENTIFIER,
                TokenClass::PRIMITIVE))
    {
        if(fromFunction){
            NodePtr retTypeNode(new Ast::Node(NODE_TYPE::FUNCTION_RETURN_TYPE,
                                             *m_iter));
            tree->addChildren(retTypeNode);
        }
        consume();
        return true;
    }

    error("Expected type for identifier declaration.",
          ERROR_LEVEL::E_ERROR);
    return false;
}


// Error reporting
void Parser::error(const std::string& args, ERROR_LEVEL elevel ) {
    // strip eof if at EOF in stream.
    m_numErrors++;
    auto stripEof = [this]() {
        return match(TokenClass::END)
              ? *(this->m_iter - 1)
              : *(this->m_iter);
    };
    // decide error level
    switch(elevel) {
        case ERROR_LEVEL::E_WARNING:
            m_logger->warn("{}", args);
            m_logger->warn("caused by {}", stripEof());
            return;
        case ERROR_LEVEL::E_ERROR:
            m_logger->error("{}", args);
            m_logger->error("caused by {}", stripEof());
            return;
        case ERROR_LEVEL::E_SEVERE:
            m_logger->critical("{} ", args);
            m_logger->critical("error at {}", stripEof());
            throw std::runtime_error("Parsing failed. See errors above.\n");
    }
}









