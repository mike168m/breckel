#include <iostream>
#include <memory>
#include <fstream>
#include <string>
#include <spdlog/spdlog.h>
#include "anyoption.h"
#include "scanner.h"
#include "parser.h"
#include "symboltablebuilder.h"

using namespace brekell;

int main( int argc, char** argv) {
    AnyOption options;
    options.setOption("input");
    options.processCommandArgs(argc, argv);

    auto console = spdlog::stdout_color_mt("error_logger");
    console->set_pattern("[Meta] %v \n");

    if (options.hasOptions()) {
        if (options.getValue("input") != nullptr) {
            std::string fileName(options.getValue("input"));
            try {
                console->info("Compiling {}", fileName);
                std::ifstream ifs(fileName, ios::in);
                std::string line;
                if (ifs.is_open()) {
                    frontend::Scanner scanner{};
                    while (getline(ifs, line)) {
                        scanner.scan(line);
                    }

                    scanner.print();
                    auto tokens = scanner.tokens();

                    frontend::Parser parser(tokens, 4);
                    parser.parse();
                    parser.print();

                    semantics::SymbolTableBuilder symTableBuilder(parser.ast());
                    symTableBuilder.build();
                    symTableBuilder.print();
                } else {
                    console->error("File not found.");
                }
            } catch(std::exception& ex) {
                console->critical("{} compilation failed.\nReason(s): \n{}",
                                  fileName, ex.what());
            }
        } else {
            console->error("No input files.\n");
        }
        console->info("Done.\n");
    }

    return 0;
}
