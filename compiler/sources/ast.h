#pragma once

#include <better_enums/enum.h>
#include <cassert>
#include "scanner.h"

namespace brekell {
namespace frontend {

BETTER_ENUM (NODE_TYPE, int,
    ROOT = 1,
    LET,
    OP_ARITHMETIC_BINARY,
    OP_BOOLEAN_BINARY,
    EXPLICIT_SCOPE,
    OP_ASSIGN,
    FUNCTION,
    FUNCTION_ARGS,
    FUNCTION_BODY,
    FUNCTION_RETURN_TYPE,
    FUNCTION_CALL,
    OP_INDEX,
    INT_LITERAL,
    STRING_LITERAL,
    IDENTIFIER,
    IDENTIFIER_DECL,
    IDENTIFIER_TYPE,
    IDENTIFIER_REFERENCE
)

struct Ast {
    struct Node {
        std::vector<Token> tokens;
        const NODE_TYPE type;
        // pointers to children
        std::vector<std::shared_ptr<Node>> children;

        explicit Node(const NODE_TYPE type):type(type){}

        template<typename First = Token, typename... Args>
        Node(const NODE_TYPE type, First first, Args... args)
            :type(type)
        {
            addTokens(first, args...);
        }

        void addTokens(void){} // handle zero argument case.
        void addTokens(const Token& token) {
            tokens.push_back(token);
        }

        void addTokens(std::vector<Token>& tokens) {
            for (auto& t : tokens) {
                this->tokens.push_back(t);
            }
        }

        template<typename First, typename... Args>
        void addTokens(First first, Args... args) {
            addTokens(first);
            addTokens(args...);
        }


        void addChildren(void){} // handle zero argument case.
        void addChildren(const std::shared_ptr<Node>& childPtr) {
            children.push_back(childPtr);
        }

        template<typename First, typename... Args>
        void addChildren(First first, Args... args) {
            addChildren(first);
            addChildren(args...);
        }

        void removeLastChild(){
            assert(children.size() > 0);
            children.pop_back();
        }

        auto lastChild() -> decltype(children.back()) {
            assert(children.size() > 0);
            return children.back();
        }

        auto firstChild() -> decltype(children.front()) {
            assert(children.size() > 0);
            return children.front();
        }

        template<typename OStream>
        friend OStream& operator<< (OStream& os, const Node &t) {
            os << t.type._to_string() << ": ";
            if (t.tokens.size() > 0) {
                os << '{';
                for (auto& token : t.tokens) {
                    os << token << ", ";
                }
                os <<"\b\b";
                os << '}';
            }
            return os;
        }
    };

    std::shared_ptr<Node> root;

    Ast():root(new Node(NODE_TYPE::ROOT)) {}
    ~Ast() = default;
};

using NodePtr = std::shared_ptr<Ast::Node>;

} // frontend
} // brekell
