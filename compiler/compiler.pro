TEMPLATE = app
CONFIG += console
CONFIG += c++1z
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += includes\
               sources\

HEADERS += sources/anyoption.h\
           sources/scanner.h \
           sources/parser.h \
           sources/ast.h \
           sources/symboltablebuilder.h\
           sources/stage.h

SOURCES += sources/anyoption.cpp \
            sources/main.cpp \
            sources/scanner.cpp \
            sources/parser.cpp

unix:CONFIG(debug, debug|release):LIBS += -L/usr/lib -pthread
unix:CONFIG(release, debug|release):LIBS += -L/usr/lib -pthread

#DISTFILES += \
#    tests/brekell/functions.brl \
#    tests/brekell/arithmetic.brl \
#    docs/context_free_grammar.txt
